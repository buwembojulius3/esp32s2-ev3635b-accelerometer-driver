/*
    Driver Firmware for MC3635 on ESP32-S2
    Platform:       ESP-IDF version 5.2.1 (Stable)
    Objective:      Power consumption test, across different operation mode configurations (Total Number of Configurations: 91)
    Autor:          Yassir Abbassi
*/

// Libraries
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

/*======================================Function Prototypes======================================*/
void mc3635_write_transaction(char op, char addr, char din, char mosi[], size_t lo, spi_transaction_t t,
                            esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void mc3635_read_transaction(char op, char addr, char mosi[], char miso[], size_t lo, size_t li,
                            spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void mc3635b_init_seq(char mosi2w[], char mosi[], char miso[], size_t lo2w, size_t lo, size_t li,
                            spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void mc3635b_hspi_enable_routine(spi_device_interface_config_t devcfg, unsigned int HSPI_Hz, char mosi[], size_t lo,
                            spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void mc3635b_cwake_rate15(char p_mode, char res_rng, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret,
                            spi_device_handle_t handle, unsigned int tick);

void mc3635b_TH_C(char mosi2w[], char mosi[], char miso[], size_t lo2w, size_t lo, size_t li, spi_transaction_t t,
                            esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

float calculate_ratio(char res, char range);

void conversionToG(float ratio, char xyzBuf[], short int XYZ_2c[], float XYZ_g[]);

void NoMotion_angle_coords(float XYZ_g[], float coords[]);

void NoMotion_spherical_coords(float coords[], float angles360[]);

void mc3635_output_display(float XYZ_g[], float coords[], float angles360[]);

void test_switcher(char OP_MODE, char P_MODE, char RES_OP[], char RNG_OP[], char ODR_OP_P[], size_t RES_count, size_t RNG_count,
                            size_t ODR_count, size_t period, int tick, char mosi2w[], char mosi[], char xyzBuf[], short int XYZ_2c[],
                            float XYZ_g[], float coords[], float angles360[], size_t lo, size_t lxyz, size_t lo2w, spi_transaction_t t,
                            esp_err_t ret, spi_device_handle_t handle, size_t *test_nbr);

void power_consumption_test_presets(char mosi2w[], char mosi[], char miso[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[],
                            float angles360[], size_t lo2w, size_t lo, size_t li, size_t lxyz, spi_transaction_t t, esp_err_t ret,
                            spi_device_handle_t handle, size_t period, unsigned int tick);
/*===============================================================================================*/

/*========================Register Map========================*/
#define MC36XX_REG_XOUT_LSB         (0x02)
#define MC36XX_REG_FREG_1           (0x0D)
#define MC36XX_REG_INIT_1           (0X0F)
#define MC36XX_REG_MODE_C           (0x10)
#define MC36XX_REG_RATE_1           (0x11)

#define MC36XX_REG_SNIFF_C          (0X12)
#define MC36XX_REG_SNIFFTH_C        (0X13)
#define MC36XX_REG_SNIFFCF_C        (0X14)
#define MC36XX_REG_RANGE_C          (0X15)
#define MC36XX_REG_FIFO_C           (0x16)
#define MC36XX_REG_INTR_C           (0x17)

#define MC36XX_REG_CHIP_ID          (0x18)
#define MC36XX_REG_INIT_3           (0X1A)
#define MC36XX_REG_PMCR             (0x1C)
#define MC36XX_REG_DMX              (0x20)
#define MC36XX_REG_DMY              (0x21)
#define MC36XX_REG_DMZ              (0x22)
#define MC36XX_REG_RESET            (0x24)
#define MC36XX_REG_INIT_2           (0X28)
#define MC36XX_REG_TRIGC            (0x29)
/*============================================================*/

/*=============================================GPIO Definition=============================================*/
// 4-Wire w/ interrupt pin SPI2
#define GPIO_INT  GPIO_NUM_2
#define GPIO_MOSI 11
#define GPIO_MISO 13
#define GPIO_SCLK 12
#define GPIO_CS 10

// Select SPI HOST
#ifdef CONFIG_IDF_TARGET_ESP32
#define SENDER_HOST HSPI_HOST
#else
#define SENDER_HOST SPI2_HOST
#endif
/*=========================================================================================================*/

/* Main application */
void app_main(void)
{
    /*====================================Bus Configurations====================================*/
    // SPI Bus Configuration: MCU 4-Wire SPI
    spi_bus_config_t buscfg = {
        .mosi_io_num = GPIO_MOSI,
        .miso_io_num = GPIO_MISO,
        .sclk_io_num = GPIO_SCLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1
    };

    // SPI Bus Configuration: SPI Slave Device
    spi_device_interface_config_t devcfg = {
        .command_bits = 0,
        .address_bits = 0,
        .dummy_bits = 0,
        .clock_speed_hz = 1000000,          // 1 MHz
        .duty_cycle_pos = 128,              // 50% duty cycle
        .mode = 0,
        .spics_io_num = GPIO_CS,
        .cs_ena_posttrans = 3,              // cs_ena_posttrans = 3 => Keep the CS low 3 cycles after transaction, to stop
        .queue_size = 3                     // slave from missing the last bit when CS has less propagation delay than CLK
    };
    /*==========================================================================================*/

    /*====================================ESP32_S2 SPI Setup====================================*/
    esp_err_t ret;
    spi_device_handle_t handle;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));

    // Initialize the SPI bus and add EV3635B slave device
    ret = spi_bus_initialize(SENDER_HOST, &buscfg, SPI_DMA_CH_AUTO);
    assert(ret == ESP_OK);
    ret = spi_bus_add_device(SENDER_HOST, &devcfg, &handle);
    assert(ret == ESP_OK);
    /*==========================================================================================*/
    
    /*============================Transaction Buffer Variables============================*/
    char sendBuf2w[2] = {0};            // sendBuf2w: MOSI transaction buffer for 2-byte writing (Burst-Writing)
    size_t lo2w = sizeof(sendBuf2w);

    char sendBuf[1] = {0};              // sendBuf: MOSI transaction buffer for single byte writing
    size_t lo = sizeof(sendBuf);

    char recvBuf[2] = {0};              // recvBuf: MOSI transaction buffer for reading
    size_t li = sizeof(recvBuf);

    char xyzBuf[7] = {0};               // recvBuf: MOSI transaction buffer for reading raw XYZ output
    size_t lxyz = sizeof(xyzBuf);

    short int XYZ_2c[3] = {0};          // XYZ_2c:  2's Complement reperesentaion of acceleration data
    float XYZ_g[3] = {0};               // XYZ_g:   Decimal conversion of acceleration data (XYZ_g = XYZ_2c / Ratio)

    // Utility Variables
    float coords[3] = {0};
    float angles360[3] = {0};

    unsigned int tick = 100;            // tick:        In milliseconds
    size_t period = 10;                 // period:      In seconds
    /*====================================================================================*/

    /*=======================================EV3635B Mode Of Operation Configuration=======================================*/
    // EV3635B SPI Initializating Sequence
    mc3635b_init_seq(sendBuf2w, sendBuf, recvBuf, lo2w, lo, li, t, ret, handle, 25);

    // Enable High Speed SPI for EV3635B
    unsigned int HSPI_Hz = 7000000;         // 7MHz HSPI clock speed
    mc3635b_hspi_enable_routine(devcfg, HSPI_Hz, sendBuf2w, lo2w, t, ret, handle, tick);

    // SNIFF Threshold
    mc3635b_TH_C(sendBuf2w, sendBuf, recvBuf, lo2w, lo, li, t, ret, handle, 25);
    /*=====================================================================================================================*/

    /*=======================================Power Consumption Test Presets Selector=======================================*/
    power_consumption_test_presets(sendBuf2w, sendBuf, recvBuf, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo2w, lo, li, lxyz,
                                    t, ret, handle, period, tick);
    /*=====================================================================================================================*/

    // END Condition Reached
    ret = spi_bus_remove_device(handle);
    assert(ret == ESP_OK);
}


/* Utility Functions */

// Write Transaction
void mc3635_write_transaction(char op, char addr, char din, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick)
{
    if (op == 0x40){
        if (lo2w < 2){
            printf("Transaction MOSI buffer length less than 2 bytes. Actual size is %zu.\n", lo2w);
            return;
        }

        mosi2w[0] = op | addr;
        mosi2w[1] = din;
        t.length = lo2w*8;
        t.tx_buffer = mosi2w;
        t.rx_buffer = NULL;
        vTaskDelay(pdMS_TO_TICKS(tick));
        ret = spi_device_transmit( handle, &(t) );

        if(ret != ESP_OK){
            printf("\tR\tADDR(0x%X)%s\n", addr, esp_err_to_name(ret));      // Error Display
        }
    }
}

// Read Transaction
void mc3635_read_transaction(char op, char addr, char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick)
{
    if (op == 0xC0){
        if (lo != 1 && li < 2){
            printf("Error in transaction buffers size. Mosi size (%zu) \t MISO size (%zu). Should be 1, and at least 2 respectively.\n", lo, li);
            return;
        }

        mosi[0] = op | addr;
        t.length = li*8;
        t.tx_buffer = mosi;
        t.rx_buffer = miso;
        vTaskDelay(pdMS_TO_TICKS(tick));
        ret = spi_device_transmit( handle, &(t) );

        if(ret == ESP_OK){
            if (false)
            {
                printf("\tR\tADDR(0x%X)", addr);
                for (size_t i = 1; i < li; i++)
                {
                    printf("\tN+%zu: OUT(0x%X)", i-1, miso[i]);
                }
                printf("\n");
            }
        }else{
            printf("\tR\tADDR(0x%X)%s\n", addr, esp_err_to_name(ret));
        }
    }
}

// EV3635B SPI Initialization Sequence
void mc3635b_init_seq(char mosi2w[], char mosi[], char miso[], size_t lo2w, size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick){
    printf("/* EV3635B SPI Initialization Sequence */\n");
    // Step 1 (Enabling Standby Mode)
    printf("// Enabling Standby Mode\n");
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x01, mosi2w, lo2w, t, ret, handle, tick);

    // Step 2 (Launch Reset)
    printf("\n//Launch Reset\n");
    mc3635_write_transaction(0x40, MC36XX_REG_RESET, 0x40, mosi2w, lo2w, t, ret, handle, tick);

    // Step 3 (Waiting for reset...)
    printf("\n// Waiting for reset...\n");
    vTaskDelay(pdMS_TO_TICKS(100));

    // Step 4 (Checking CHIP_ID for non zero Chip ID)
    printf("\n// Checking CHIP_ID for non zero Chip ID\n");
    mc3635_read_transaction(0xC0, MC36XX_REG_CHIP_ID, mosi, miso, lo, li, t, ret, handle, tick);
    if ((int)miso[1] != 0){
        printf("\tNon Zero CHIP ID\t0x%X\n", (int)miso[1]);
    }
    
    // Step 5 (Enabling SPI Mode)
    printf("\n// Enabling SPI Mode\n");
    mc3635_write_transaction(0x40, MC36XX_REG_FREG_1, 0x80, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_read_transaction(0xC0, MC36XX_REG_FREG_1, mosi, miso, lo, li, t, ret, handle, tick);

    // Step 6 (Checking FREG_1)
    printf("\n// Checking FREG_1\n");
    if ((int)miso[1] != 0x80){
        int n = 1;
        while ((int)miso[1] != 0x80 && n != 10){
            n++;
            printf("ERROR:\t0x0D value is not valid for SPI COMM, writing attempt no %d\tPlease Standby...\n", n);
            mc3635_write_transaction(0x40, MC36XX_REG_FREG_1, 0x80, mosi2w, lo2w, t, ret, handle, 200);
            mc3635_read_transaction(0xC0, MC36XX_REG_FREG_1, mosi, miso, lo, li, t, ret, handle, 200);
        }
        printf("\n10 attempts have been made. Exit execution now.\n");
        ret = spi_bus_remove_device(handle);
        assert(ret == ESP_OK);
        esp_restart();
        return;
    }
    printf("\tMC36XX_REG_FREG_1 is set to 0x80\n");
    
    // Step 7 (Initializating INIT_1)
    printf("\n// Initializating INIT_1\n");
    mc3635_write_transaction(0x40, MC36XX_REG_INIT_1, 0x42, mosi2w, lo2w, t, ret, handle, tick);
    
    // Step 8 (Enabling Standby from Sleep)
    printf("\n// Enabling Standby from Sleep\n");
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x01, mosi2w, lo2w, t, ret, handle, tick);
    
    // Step 9 (Waiting for state machine...)
    printf("\n// Waiting for state machine...\n");
    vTaskDelay(pdMS_TO_TICKS(100));
    
    // Step 10-14 (Initializating DMX, DMY, INIT_2 & INIT_3)
    printf("\n// Initializating DMX, DMY, INIT_2 & INIT_3\n");
    mc3635_write_transaction(0x40, MC36XX_REG_DMX, 0x01, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_DMY, 0x80, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_INIT_2, 0x00, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_INIT_3, 0x00, mosi2w, lo2w, t, ret, handle, tick);
}

// Enable HSPI Clock Speed
void mc3635b_hspi_enable_routine(spi_device_interface_config_t devcfg, unsigned int HSPI_Hz, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick){
    if (HSPI_Hz > 8000000)
    {
        printf("The new SPI clock speed value is invalid for MC3635. (Higher than 8MHz)\n");
        return;
    }

    // Enble HSPI
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_PMCR, 0x80, mosi2w, lo2w, t, ret, handle, tick);

    ret = spi_bus_remove_device(handle);                            // Remove MC3635 from bus
    assert(ret == ESP_OK);

    devcfg.clock_speed_hz = HSPI_Hz;                                // Change clock speed

    ret = spi_bus_add_device(SENDER_HOST, &devcfg, &handle);        // Add EV3635B again to bus
    assert(ret == ESP_OK);
}

// Enable Rate15 for CWake
void mc3635b_cwake_rate15(char p_mode, char res_rng, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick){
    char p_15odr_cfg[2];
    char selected_power = (p_mode & 0x7);

    if (selected_power == 0x3)
    {
        p_15odr_cfg[0] = 0x52;
        p_15odr_cfg[1] = 0x1;

    }else if (selected_power == 0x0)
    {
        p_15odr_cfg[0] = 0x72;
        p_15odr_cfg[1] = 0x2;

    }else if (selected_power == 0x4)
    {
        p_15odr_cfg[0] = 0x32;
        p_15odr_cfg[1] = 0x12;
        
    }else
    {
        printf("\t\tError: Can't set Rate15 for CWake mode.\n");
        return;
    }

    // The following is Rate15 settings for CWake Mode. The setup steps are explained in slave device (EV3635B) datasheet
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x1, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RANGE_C, res_rng, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_FIFO_C, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_PMCR, selected_power, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, 0x10, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_TRIGC, 0x3, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, 0x20, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_TRIGC, 0x1, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, 0x40, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_TRIGC, p_15odr_cfg[0], mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, 0x50, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_TRIGC, p_15odr_cfg[1], mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, 0xF, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x5, mosi2w, lo2w, t, ret, handle, tick);
}

// EV3635B SNIFF XYZ Threshold
void mc3635b_TH_C(char mosi2w[], char mosi[], char miso[], size_t lo2w, size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick){
    // Set X Threshold
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFCF_C, 0x21, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFTH_C, 0x3, mosi2w, lo2w, t, ret, handle, tick);

    // Set Y Threshold
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFCF_C, 0x22, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFTH_C, 0x3, mosi2w, lo2w, t, ret, handle, tick);

    // Set Z Threshold
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFCF_C, 0x23, mosi2w, lo2w, t, ret, handle, tick);
    mc3635_write_transaction(0x40, MC36XX_REG_SNIFFTH_C, 0x3, mosi2w, lo2w, t, ret, handle, tick);
}

// Ratio Calculation
float calculate_ratio(char resolution, char range){
    float ratio;

    // Determine the base ratio based on the resolution chosen value
    switch (resolution){
        case 0x0:
            ratio = 64.0f;
            break;
        case 0x1:
            ratio = 128.0f;
            break;
        case 0x2:
            ratio = 256.0f;
            break;
        case 0x3:
            ratio = 1024.0f;
            break;
        case 0x4:
            ratio = 2048.0f;
            break;
        case 0x5:
            ratio = 4096.0f;
            break;
        default:
            // Handle invalid res value
            return 0.0f;
    }

    // Adjust the ratio based on the range chosen value
    switch (range){
        case 0x0:
            ratio /= 2.0f;
            break;
        case 0x10:
            ratio /= 4.0f;
            break;
        case 0x20:
            ratio /= 8.0f;
            break;
        case 0x30:
            ratio /= 16.0f;
            break;
        case 0x40:
            ratio /= 12.0f;
            break;
        default:
            // Handle invalid range value
            return 0.0f;
    }

    return ratio;
}

// Angles in Degrees (No Motion Case)
void NoMotion_angle_coords(float XYZ_g[], float coords[]){
    // Angle Coordinates for X axis
    if (XYZ_g[0] == 1 || XYZ_g[0] == -1)
    {
        coords[0] = XYZ_g[0];
    }else
    {
        coords[0] = fmodf(XYZ_g[0], 1.0f);
    }

    // Angle Coordinates for Y axis
    if (XYZ_g[1] == 1 || XYZ_g[1] == -1)
    {
        coords[1] = XYZ_g[1];
    }else
    {
        coords[1] = fmodf(XYZ_g[1], 1.0f);
    }

    // Angle Coordinates for Z axis
    if (XYZ_g[2] == 1 || XYZ_g[2] == -1)
    {
        coords[2] = XYZ_g[2];
    }else
    {
        coords[2] = fmodf(XYZ_g[2], 1.0f);
    }
}

// 360 Degrees convertion of angles computed by NoMotion_angle_coords()
void NoMotion_spherical_coords(float coords[], float angles360[]){
    angles360[0] = (180.0/M_PI) * (atan2(-coords[1]*90, -coords[2]*90) + M_PI);
    angles360[1] = (180.0/M_PI) * (atan2(-coords[0]*90, -coords[2]*90) + M_PI);
    angles360[2] = (180.0/M_PI) * (atan2(-coords[1]*90, -coords[0]*90) + M_PI);
}

// Acceleration Calculation
void conversionToG(float ratio, char xyzBuf[], short int XYZ_2c[], float XYZ_g[]){
    XYZ_2c[0] = ( xyzBuf[1] + (xyzBuf[2] << 8) );
    XYZ_2c[1] = ( xyzBuf[3] + (xyzBuf[4] << 8) );
    XYZ_2c[2] = ( xyzBuf[5] + (xyzBuf[6] << 8) );
    XYZ_g[0] = XYZ_2c[0] / ratio;
    XYZ_g[1] = XYZ_2c[1] / ratio;
    XYZ_g[2] = XYZ_2c[2] / ratio;
}

// Output Display function
void mc3635_output_display(float XYZ_g[], float coords[], float angles360[]){
    // Euler Angles
    NoMotion_angle_coords(XYZ_g, coords);
    NoMotion_spherical_coords(coords, angles360);
    //printf("Euler Angles: \t%.2f\t%.2f\t%.2f\n", coords[0]*90, coords[1]*90, coords[2]*90);
    //printf("360 Angles: \t%.2f\t%.2f\t%.2f\n", angles360[0], angles360[1], angles360[2]);
    
    // Acceleration
    printf("Acceleration: \t%.2fg\t%.2fg\t%.2fg\n", XYZ_g[0], XYZ_g[1], XYZ_g[2]);
}

// Test Configuration Switcher
void test_switcher(char OP_MODE, char P_MODE, char RES_OP[], char RNG_OP[], char ODR_OP_P[], size_t RES_count, size_t RNG_count, size_t ODR_count, size_t period, int tick, char mosi2w[], char mosi[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[], float angles360[], size_t lo, size_t lxyz, size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, size_t *test_nbr){
    
    // Loop for output display's variables
    size_t count = period * (1000/tick);             // If tick = 200, then count = 20 * (1000/tick) = 100 readings
    float ratio;

    /*=========================================String Indicator for Selected Test Preset=========================================*/
    char Operational_Mode[8];           // MC36XX_REG_MODE_C
    char Power_Mode[16];                // MC36XX_REG_PMCR
    char RES[7];                        // MC36XX_REG_RANGE_C
    char RNG[7];                        // MC36XX_REG_RANGE_C
    /*===========================================================================================================================*/

    switch (OP_MODE){
    case 0x1:
        strcpy(Operational_Mode, "Standby");
        break;

    case 0x2:
        strcpy(Operational_Mode, "Sniff");
        break;

    case 0x5:
        strcpy(Operational_Mode, "CWake");
        break;

    case 0x6:
        strcpy(Operational_Mode, "SWake");
        break;
        
    default:
        strcpy(Operational_Mode, "ERR");
        break;
    }

    switch (P_MODE){
    case 0x0:
        strcpy(Power_Mode, "Low Power");
        break;

    case 0x33:
        strcpy(Power_Mode, "Ultra-Low Power");
        break;

    case 0x44:
        strcpy(Power_Mode, "Precision");
        break;
        
    default:
        strcpy(Power_Mode, "ERR");
        break;
    }


    /*======================================================Test Loop======================================================*/
    // Switching to sleep mode is required before the start of test
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x0, mosi2w, lo2w, t, ret, handle, 25);

    // Loop over resolutions
    for (size_t u = 0; u < RES_count; u++)
    {
        switch (RES_OP[u])
        {
        case 0x0:
            strcpy(RES, "6-bit");
            break;

        case 0x1:
            strcpy(RES, "7-bit");
            break;

        case 0x2:
            strcpy(RES, "8-bit");
            break;

        case 0x3:
            strcpy(RES, "10-bit");
            break;

        case 0x4:
            strcpy(RES, "12-bit");
            break;

        case 0x5:
            strcpy(RES, "14-bit");
            break;
        
        default:
            strcpy(RES, "ERR");
            break;
        }

        // Loop over ranges
        for (size_t v = 0; v < RNG_count; v++)
        {
            // Writing transaction for setting acceleration resolution and range
            mc3635_write_transaction(0x40, MC36XX_REG_RANGE_C, RES_OP[u] + RNG_OP[v], mosi2w, lo2w, t, ret, handle, 25);
            ratio = calculate_ratio(RES_OP[u], RNG_OP[v]);

            switch (RNG_OP[v])
            {
            case 0x0:
                strcpy(RNG, "+-2 G");
                break;

            case 0x10:
                strcpy(RNG, "+-4 G");
                break;

            case 0x20:
                strcpy(RNG, "+-8 G");
                break;

            case 0x30:
                strcpy(RNG, "+-16 G");
                break;

            case 0x40:
                strcpy(RNG, "+-12 G");
                break;
            
            default:
                strcpy(RNG, "ERR");
                break;
            }

            // Loop over ODRs
            for (size_t w = 0; w < ODR_count; w++)
            {
                // Switch ODR Addresses
                if (OP_MODE == 0x1 || OP_MODE == 0x2)
                {
                    // Writing transaction for setting ODR in case of sniff or standby modes
                    mc3635_write_transaction(0x40, MC36XX_REG_SNIFF_C, ODR_OP_P[w], mosi2w, lo2w, t, ret, handle, 25);

                }else if (OP_MODE == 0x5 || OP_MODE == 0x6)
                {
                    // Writing transaction for setting ODR in case of wake modes
                    if (ODR_OP_P[w] != 0xF)
                    {
                        mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, ODR_OP_P[w], mosi2w, lo2w, t, ret, handle, 25);
                    }else
                    {
                        // !!!!!!!!!!!!!!       ACCORDING TO DOCUMENTATION, Rate 0xF requires additional setup steps which are ignored in the current instruction        !!!!!!!!!!!!!!
                        mc3635_write_transaction(0x40, MC36XX_REG_RATE_1, ODR_OP_P[w], mosi2w, lo2w, t, ret, handle, 25);
                    }

                }else
                {
                    // Switch to Sleep
                    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x0, mosi2w, lo2w, t, ret, handle, 25);
                    break;
                }
                    
                // Writing operation for setting operational mode. THIS ALWAYS SHOULD BE THE LAST TRANSACTION
                mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, OP_MODE, mosi2w, lo2w, t, ret, handle, 25);

                // Reading Loop
                printf("\n\n\tTest Configs %zu: %s | %s | %s | %s | 0x%02X\n", *test_nbr, Operational_Mode, Power_Mode, RES, RNG, ODR_OP_P[w]); (*test_nbr)++; 
                if (OP_MODE == 0x5 || OP_MODE == 0x6)
                {
                    for (size_t x = 0; x < count; x++)
                    {
                        mc3635_read_transaction(0xC0, MC36XX_REG_XOUT_LSB, mosi, xyzBuf, lo, lxyz, t, ret, handle, tick);
                        conversionToG(ratio, xyzBuf, XYZ_2c, XYZ_g);
                        //mc3635_output_display(XYZ_g, coords, angles360);            // Optional
                    }
                }else
                {
                    for (size_t y = 0; y < count; y++)
                    {
                        vTaskDelay(pdMS_TO_TICKS(tick));
                    }
                }

                // Switching to sleep mode is required after every test configuration
                mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x0, mosi2w, lo2w, t, ret, handle, 25);  
            }   
        }   
    }
    /*=====================================================================================================================*/
}

// Power Modes Test
void power_consumption_test_presets(char mosi2w[], char mosi[], char miso[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[], float angles360[], size_t lo2w, size_t lo, size_t li, size_t lxyz, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, size_t period, unsigned int tick){
    
    /*======================================================Preset Test Variables Array======================================================*/
    // All Operational Modes
    char OP_MODE[4] = {0x1, 0x2, 0x5, 0x6};                             // Operational Modes: {Standby, Sniff, Cwake, Swake}
    /*                                                                                           STB     SNF    CWK    SWK*/

    // All Power Modes
    char P_MODE[3] = {0x0, 0x33, 0x44};                                 // Power Modes: {Low Power, Ultra-Low Power, Precision}
    /*                                                                                       LP            ULP          P*/

    // Standby ODR per Power Modes                                      // STB ODR for ULP, LP, and P power modes respectively
    char ODR_STB_LP[2] = {0x40, 0x60};                                  // {3, 6}                                                   (Hz)
    char ODR_STB_ULP[2] = {0x20, 0x40};                                 // {3, 5}                                                   (Hz)
    char ODR_STB_P[3] = {0x80, 0xA0, 0xC0};                             // {1.5, 3, 5}                                              (Hz)
    char RES_STB[1] = {0x4};                                            // Resolutions for STB: {12}                                (bit)
    char RNG_STB[1] = {0x30};                                           // Ranges for STB: {+-16}                                   (G)

    // Sniff ODRs per Power Modes & Ranges                              // SNF ODR for ULP, LP, and P power modes respectively
    char ODR_SNF_LP[8] = {0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA};      // {1.5, 7, 14, 28, 54, 105, 210, 400}                      (Hz)
    char ODR_SNF_ULP[7] = {0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9};          // {1.5, 6, 13, 25, 50, 100, 190}                           (Hz)
    char ODR_SNF_P[4] = {0x4, 0x5, 0x6, 0x7};                           // {7, 14, 28, 55}                                          (Hz)
    char RES_SNF[1] = {0x4};                                            // Resolutions for SNF: {12}                                (bit)
    char RNG_SNF[1] = {0x30};                                           // Ranges for SNF: {+- 16}                                  (G)

    // CWake/SWake ODRs per Power Modes & Ranges                        // C/SWake ODR for ULP, LP, and P power modes respectively
    char ODR_WAK_LP[7] = {0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB};           // {14, 28, 54, 105, 210, 400, 600}                         (Hz)
    char ODR_WAK_ULP[5] = {0x6, 0x7, 0x8, 0x9, 0xC};                    // {25, 50, 100, 190, 1100}                                 (Hz)
    char ODR_WAK_P[4] = {0x5, 0x6, 0x7, 0xF};                           // {14, 28, 55, 100}                                        (Hz)
    char RES_WAK[1] = {0x4};                                            // Resolutions for C/SWake: {12}                            (bit)
    char RNG_WAK[2] = {0x30, 0x0};                                      // Ranges for C/SWake: {+-16, +-2}                          (G)
    /*=======================================================================================================================================*/
    
    // Analysis Time Measurment
    TickType_t start_tick = xTaskGetTickCount();                        // Get start tick counts
    size_t test_nbr = 1;                                                // Initialize number of tests

    // Sleep
    mc3635_write_transaction(0x40, MC36XX_REG_MODE_C, 0x0, mosi2w, lo2w, t, ret, handle, 25);
    printf("\n\n\tTest Configs %zu: Sleep | No Configuration\n", test_nbr);
    for (size_t i = 0; i < 10; i++)
    {
        // If tick = 200, then i_max = 20 * (1000/tick) = 100
        vTaskDelay(pdMS_TO_TICKS(tick));
    }
    test_nbr++;

    // Standby
    test_switcher(OP_MODE[0], P_MODE[0], RES_STB, RNG_STB, ODR_STB_LP, sizeof(RES_STB), sizeof(RNG_STB), sizeof(ODR_STB_LP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[0], P_MODE[1], RES_STB, RNG_STB, ODR_STB_ULP, sizeof(RES_STB), sizeof(RNG_STB), sizeof(ODR_STB_ULP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[0], P_MODE[2], RES_STB, RNG_STB, ODR_STB_P, sizeof(RES_STB), sizeof(RNG_STB), sizeof(ODR_STB_P), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);

    // Sniff
    test_switcher(OP_MODE[1], P_MODE[0], RES_SNF, RNG_SNF, ODR_SNF_LP, sizeof(RES_SNF), sizeof(RNG_SNF), sizeof(ODR_SNF_LP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[1], P_MODE[1], RES_SNF, RNG_SNF, ODR_SNF_ULP, sizeof(RES_SNF), sizeof(RNG_SNF), sizeof(ODR_SNF_ULP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[1], P_MODE[2], RES_SNF, RNG_SNF, ODR_SNF_P, sizeof(RES_SNF), sizeof(RNG_SNF), sizeof(ODR_SNF_P), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    
    // Cwake
    test_switcher(OP_MODE[2], P_MODE[0], RES_WAK, RNG_WAK, ODR_WAK_LP, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_LP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[2], P_MODE[1], RES_WAK, RNG_WAK, ODR_WAK_ULP, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_ULP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[2], P_MODE[2], RES_WAK, RNG_WAK, ODR_WAK_P, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_P), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    
    // Swake
    test_switcher(OP_MODE[3], P_MODE[0], RES_WAK, RNG_WAK, ODR_WAK_LP, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_LP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[3], P_MODE[1], RES_WAK, RNG_WAK, ODR_WAK_ULP, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_ULP), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);
    test_switcher(OP_MODE[3], P_MODE[2], RES_WAK, RNG_WAK, ODR_WAK_P, sizeof(RES_WAK), sizeof(RNG_WAK), sizeof(ODR_WAK_P), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);

    // Print Total Analysis Time
    TickType_t end_tick = xTaskGetTickCount();                          // Get end tick count
    TickType_t elapsed_ticks = end_tick - start_tick;                   // Calculate elapsed ticks
    uint32_t elapsed_time_ms = elapsed_ticks * portTICK_PERIOD_MS;      // Convert ticks to milliseconds
    
    int minutes = elapsed_time_ms / (60 * 1000);                        // Extract minutes
    int seconds = (elapsed_time_ms % (60 * 1000)) / 1000;               // Extract seconds
    int milliseconds = elapsed_time_ms % 1000;                          // Extract milliseconds
    printf("\n\n\t===== Elapsed time: %d mins, %d secs, %d msecs =====\n\n", minutes, seconds, milliseconds);
}